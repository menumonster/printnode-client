var PrintNode = require('../index'),
	path = require('path'),
	fs = require('fs'),
	foreach = require('generator-foreach'),
	rootPath = path.normalize(__dirname + '/..');

var client = new PrintNode({ api_key: "e884f5587fdb75b15921d1e5eaa839f17d66a53c" });

describe('Test client with printer from Mikes Laptop', function () {
	this.timeout(10000);

	// it('should see label printer attached to mikes laptop', function* () {
	// 	var printer = yield client.findPrinter({ computer: { name: "MIKE-PC-SAMSUNG" }, name: "Label Printer" });
	// });

	it('should print a label', function* () {
		var content = client.base64_encode(rootPath + "/test/examples/label.pdf");

		var jobs = [{
			printer: 83180,
			title: "Test print job " + Date.now(),
			source: "Get Kai Supplier Interface 1", // device name
			contentType: "pdf_base64",
			content: content
		},{
			printer: 83180,
			title: "Test print job " + Date.now(),
			source: "Get Kai Supplier Interface 1", // device name
			contentType: "pdf_base64",
			content: content
		}];
		
		yield client.createPrintJobs(jobs);
	});
});