var request = require('request-promise'),
	_ = require('lodash'),
	fs = require('fs');

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

var PrintNode = function (options) {
	this.api_key = options.api_key;
	this.printer_id = options.printer_id;
	this.version = "~3";
  this.base_url = "https://" + this.api_key + ":@api.printnode.com/";
};

PrintNode.prototype.base64_encode = base64_encode;

PrintNode.prototype.whoami = function* () {
	if(this.whoami) { return this.whoami; }
	this.whoami = yield request({
		url: this.base_url + "whoami",
		json: true
	});
	return whoami;
};

PrintNode.prototype.findPrinters = function* () {
	this.printers = yield request({ 
		url: this.base_url + "printers",
		json: true
	});
	return this.printers;
};

PrintNode.prototype.findPrinter = function* (options) {
	var printers = yield this.findPrinters();
	var printer = _.findWhere(printers, options);
	if( !printer ) {
		throw new Error('Printer not found for conditions: ' + JSON.stringify(options));
	}

	this.printer_id = printer.id;
	return printer;
};

PrintNode.prototype._jobs = {};

PrintNode.prototype.createPrintJob = function (options) {
	if( !this.printer_id && !options.printer) {
		throw new Error('Must selected a printer');
	}

	if( !options.printer ) {
		options.printer = this.printer_id;
	}

	var start = Date.now();
	if( options.filename ) {
		options.content = base64_encode(options.filename);
		delete options.filename;
	}

	start = Date.now()
	var xhr_options = { 
		url: this.base_url + "printjobs",
		json: true,
		method: "POST",
		body: options
	};

	return request(xhr_options);
};

PrintNode.prototype.createPrintJobs = function (jobs) {
	var start = Date.now();

	var promises = jobs.map(function (job) {
		return this.createPrintJob(job);
	}.bind(this));

	return Promise.all(promises);
};

module.exports = PrintNode;
